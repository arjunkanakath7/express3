const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const productsRoutes = require('./routes/productsRoutes')
const categoryRoutes = require('./routes/categoryRoutes')

app.use(cors())
app.use('/products', productsRoutes)
app.use('/categories', categoryRoutes)


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})