const express = require('express')
const { getcategorybyid, getallcategories, addcategory, updatecategory, deletecategory } = require('../controllers/categorycontroller')
const router = express.Router()

  router.get('/', getallcategories)

  router.get('/:categoryID', getcategorybyid)

  router.post('/', addcategory)

  router.patch('/:categoryID', updatecategory)

  router.delete('/:categoryID',deletecategory)

module.exports = router