const express = require('express')
const { getallproducts, getproductbyid, addnewproduct, updateproduct, deleteaproduct } = require('../controllers/productcontroller')
const router = express.Router()

//1. Get all products
router.get('/', getallproducts)
  
  //2. Get product by ID
  router.get('/:productID',getproductbyid)
  
    //3. Add new products
    router.post('/', addnewproduct)
  
  //4. update products
  router.patch('/:productID', updateproduct)
  
    //5. Delete a product
    router.delete('/:productID',deleteaproduct )

module.exports = router